// Example of a all-in-Lua application using Celdev Responsive Development Environment
// AppDelegate.m for an Xcode iOS "Empty Application" project
// Created by Jean-Luc Jumpertz - Celedev on 20/10/2013

#import "LCVAppDelegate.h"

#import "CIMLua/CIMLua.h"
#import "CIMLua/CIMLuaRemoteMonitor.h"

@interface LCVAppDelegate ()
{
    CIMLuaContext* _collectionLuaContext;
    CIMLuaRemoteMonitor* _collectionLuaContextMonitor;
}

@end

@implementation LCVAppDelegate

+(void)load
{
    // Declare base classes that shall be executed in the application's main thread
    [CIMLuaContext declareObjcMethodsInvocationContext:dispatch_get_main_queue() forClasses:[UIView class], [UIViewController class], [UICollectionViewLayout class], nil];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Create a Lua Context for this application
    _collectionLuaContext = [[CIMLuaContext alloc] initWithName:@"Collection Control"];
    
    // Create a Remote Monitor associated with this context, for enabling the connection with Celedev IDE
    _collectionLuaContextMonitor = [[CIMLuaRemoteMonitor alloc] initWithLuaContext:_collectionLuaContext connectionTimeout:30];
    
    // Create the application window (standard stuff)
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    // Run the code for this Lua context
    [_collectionLuaContext loadLuaModuleNamed:@"CollectionControllerMain" withCompletionBlock:^(id result) {
        
        if ([result isKindOfClass:[UIViewController class]])
        {
            self.window.rootViewController = result;
        }
    }];
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
