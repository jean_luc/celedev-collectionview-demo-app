--[[ Default application's start Lua module 

     Its presence inside an under-development application bundle ensures that the initial call to 
     -[CIMLuaContext loadLuaModuleNamed:withCompletionBlock:] will succeed, even if the IDE is not
     available when the application starts. 
     If the connection to the IDE is established at some later time, this module will be reloaded
     and replaced by the corresponding program-specific version found on the host computer.
  ]]

return nil