// Example of a all-in-Lua application using Celdev Responsive Development Environment
// AppDelegate.h for an Xcode iOS "Empty Application" project
// Created by Jean-Luc Jumpertz - Celedev on 20/10/2013

#import <UIKit/UIKit.h>

@interface LCVAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
