# Celedev CollectionView Demo app

Celedev-CollectionView-Demo is an iOS application written entirely in Lua, that has been used to demonstrate Celedev Responsive Programming environment during the presentations I did at the CocoaHeads Rennes session #14 and at Lua Workshop 2013 in Toulouse.

This repo contains both the Lua and Objective C source code of this application. 
The code is short and has many comments in it, so it should be really easy to understand if you are familiar with either Lua or iOS.

The slides of the CocoaHeads Rennes presentation (in French) can be found at: [www.celedev.com/download/CocoaHeads_Rennes_14_Celedev.pdf](http://www.celedev.com/download/CocoaHeads_Rennes_14_Celedev.pdf)

The slides of the Lua Workshop presentation can be found at: [www.celedev.com/download/Lua_workshop_2013_Celedev.pdf](http://www.celedev.com/download/Lua_workshop_2013_Celedev.pdf)

For more information, you can have a look at Celedev's website [www.celedev.com](http://www.celedev.com)