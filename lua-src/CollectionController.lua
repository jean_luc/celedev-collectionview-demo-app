-- CollectionController: the UICollectionViewController for this collection view

-- Celedev Collectionview demo app
-- Example of a all-in-Lua iOS application using Celdev Responsive Development Environment
-- Created by Jean-Luc Jumpertz - Celedev on 20/10/2013


require "UIKit.UIGestureRecognizer"

-- Load component classes
local CollectionViewLayout = require "PinchFlowLayout"
local ViewCellClass        = require "LabelCell"

local UICollectionViewController = objc.UICollectionViewController

-- Create a CollectionController class subclassing UICollectionViewController
local CollectionController = class.createClass ("CollectionController", UICollectionViewController)

function CollectionController:init ()
    -- call super
    self = self:doAsClass ("initWithCollectionViewLayout", UICollectionViewController, CollectionViewLayout:new())
    return self
end  

function CollectionController:configureCollectionView ()
    
    local collectionView = self.collectionView
    collectionView.collectionViewLayout.itemSize = { width = 184, height = 100 }
end

local cellCount = 200

-- UIViewController method

function CollectionController:viewDidLoad ()
    
    local collectionView = self.collectionView
    
    -- register the cell class
    collectionView:registerClass_forCellWithReuseIdentifier (ViewCellClass, "LABEL_CELL")
    
    -- Create a pinch gesture recognizer
    local pinchRecognizer = objc.UIPinchGestureRecognizer:newWithTarget_action (self, "handlePinchGesture")
    pinchRecognizer.delegate = self
    collectionView:addGestureRecognizer (pinchRecognizer)
    self.pinchRecognizer = pinchRecognizer
    
    -- Create a rotation gesture recognizer
    local rotationRecognizer = objc.UIRotationGestureRecognizer:newWithTarget_action (self, "handleRotationGesture")
    rotationRecognizer.delegate = self
    collectionView:addGestureRecognizer (rotationRecognizer)
    self.rotationRecognizer = rotationRecognizer
    
    -- configure the collection view
    self:configureCollectionView ()
end

-- Collection view data source methods

function CollectionController:collectionView_numberOfItemsInSection (view, section)
    return cellCount;
end  

function CollectionController:collectionView_cellForItemAtIndexPath (collectionView, indexPath)
    
    -- get a cell from the collection view
    local cell = collectionView:dequeueReusableCellWithReuseIdentifier_forIndexPath ("LABEL_CELL", indexPath);
    
    local cellIndex = indexPath.item
    
    -- configure the cell appearance
    cell:setAppearance (cellIndex, cellCount)
    
    -- set the cell text (Lua strings will be automatically converted to Objective-C NSString)
    cell.label.text = "? " .. tostring(cellIndex + 1)
    
    -- and return the cell
    return cell
end

-- Gesture recognizers

-- This gets a Lua-ish version of the UIGestureRecognizerState enum,
-- so we can use the enum values from Lua. E.g. UIGestureRecognizerState.Began
local UIGestureRecognizerState = iOS.UIKit.UIGestureRecognizer.State

function CollectionController:handlePinchGesture (gestureRecognizer)
    
    local layout = self.collectionView.collectionViewLayout
    
    if gestureRecognizer.state == UIGestureRecognizerState.Began then
        
        -- We can transparently use any C / Objective C type from Lua
        local initialPinchPoint = gestureRecognizer:locationInView (self.collectionView)
        layout.pinchedCellPath = self.collectionView:indexPathForItemAtPoint (initialPinchPoint)
        
    elseif gestureRecognizer.state == UIGestureRecognizerState.Changed then
        
        -- This calls the pinchedCellScale setter of our PinchFlowLayout class
        layout.pinchedCellScale  = gestureRecognizer.scale
        
        -- We could have written this: if gestureRecognizer.numberOfTouches > 1 then
        if gestureRecognizer:numberOfTouches() > 1 then
            layout.pinchedCellCenter = gestureRecognizer:locationInView (self.collectionView)
        end
        
    else
        -- We simply pass Lua functions for Objective C blocks parameters
        self.collectionView:performBatchUpdates_completion (function ()
                                                                layout.pinchedCellScale = nil
                                                                layout.pinchedCellCenter = nil
                                                            end,
                                                            function (finished)
                                                                layout.pinchedCellPath = nil
                                                            end)
    end
end

-- This makes the action method CollectionController:handlePinchGesture callable from Objective C
CollectionController:publishActionMethod ("handlePinchGesture")

function CollectionController:handleRotationGesture (gestureRecognizer)
    
    local layout = self.collectionView.collectionViewLayout
    
    if gestureRecognizer.state == UIGestureRecognizerState.Changed then
        
        layout.rotationAngle  = gestureRecognizer.rotation
        
    elseif gestureRecognizer.state ~= UIGestureRecognizerState.Began then
        
        self.collectionView:performBatchUpdates_completion (function () 
                                                                layout.rotationAngle = nil 
                                                            end, 
                                                            nil)
    end
end

CollectionController:publishActionMethod ("handleRotationGesture")

-- Protocol UIGestureRecognizerDelegate

function CollectionController:gestureRecognizer_shouldRecognizeSimultaneouslyWithGestureRecognizer(recognizer1, recognizer2)
    return ((recognizer1 == self.pinchRecognizer) and (recognizer2 == self.rotationRecognizer)) or
            ((recognizer2 == self.pinchRecognizer) and (recognizer1 == self.rotationRecognizer))
end

-- This declares to the Objective C runtime that the Lua Class CollectionController conforms to 
-- the UIGestureRecognizerDelegate protocol, and makes every CollectionController method declared in 
-- this protocol callable from Objective C
CollectionController:publishObjcProtocols {"UIGestureRecognizerDelegate"}

-- Refresh the CollectionView display when any code module is updated

function CollectionController:refresh ()
   
   local collectionView = self.collectionView   
   
    if collectionView ~= nil then
        self:configureCollectionView ()
        collectionView:reloadData()  
        collectionView.collectionViewLayout:invalidateLayout()
   end
   
end  

-- return the CollectionController class as expected by Lua function require()
return CollectionController;
