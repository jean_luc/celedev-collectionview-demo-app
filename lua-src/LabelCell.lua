-- LabelCell: a specific UICollectionViewCell with a Text label in it

-- Celedev Collectionview demo app
-- Example of a all-in-Lua iOS application using Celdev Responsive Development Environment
-- Created by Jean-Luc Jumpertz - Celedev on 20/10/2013

local UICollectionViewCell = objc.UICollectionViewCell

local UIView = require "UIKit.UIView"
local NSText = require "UIKit.NSText"

-- Create a LabelCell class subclassing UICollectionViewCell
local Cell = class.createClass ("LabelCell", UICollectionViewCell)

-- Cell initializer
function Cell:initWithFrame (frame)
    
    -- Call super
    self = self:doAsClass ("initWithFrame", UICollectionViewCell, frame)
    
    if self ~= nil then
        -- configure the cell's content and appearance
        self:setAppearance()
    end    
    
    return self    
end  

-- Define some locals to reference Objective C classes
local UILabel = objc.UILabel
local UIFont = objc.UIFont
local UIColor = objc.UIColor

-- Cell:setAppearance

function Cell:setAppearance (cellIndex, cellCount)
    
    -- ensure that params are not nil
    cellIndex, cellCount = cellIndex or 0, cellCount or 1
    
    local contentView = self.contentView
    local contentSize = contentView:bounds().size
    
    -- Text label
    local label = self.label
    
    if label == nil then
        -- create the label
        label = UILabel:newWithFrame(contentView.bounds)
        -- configure it (note the easy access to iOS SDK enums and constants)
        label.autoresizingMask = UIView.Autoresizing.FlexibleHeight + UIView.Autoresizing.FlexibleWidth
        label.textAlignment = NSText.Alignment.Center
        -- add the label to the contentView
        contentView:addSubview (label)
        self.label = label
    end
    
    -- Set the label's frame rectangle as a Lua table (will be automatically converted to a CGRect struct internally)
    label.frame = { x = 0, y = contentSize.height / 4, width = contentSize.width, height = contentSize.height / 2 }
    label.font = UIFont:boldSystemFontOfSize (46.0)
    label.backgroundColor = UIColor.clearColor
    label.shadowColor = UIColor.darkGrayColor
    label.textColor = UIColor.whiteColor
    
    -- Content view
    contentView.clipsToBounds = true
    contentView.layer.borderWidth = 10
    
    -- Cell colors
    local cellHue = (cellIndex / cellCount + 0.3) % 1.0
    contentView.backgroundColor = UIColor:colorWithHue_saturation_brightness_alpha (cellHue, 0.4, 0.8, 1)
    contentView.layer.borderColor = UIColor:colorWithHue_saturation_brightness_alpha (cellHue, 0.9, 0.8, 1).CGColor
end

return Cell;