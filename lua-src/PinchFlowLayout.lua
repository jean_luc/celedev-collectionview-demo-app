-- PichFlowLayout: a customized UICollectionViewFlowLayout for the collection view 

-- Celedev Collectionview demo app
-- Example of a all-in-Lua iOS application using Celdev Responsive Development Environment
-- Created by Jean-Luc Jumpertz - Celedev on 20/10/2013

local CATransform3D = require "QuartzCore.CATransform3D"
local math = math

local UICollectionViewFlowLayout = objc.UICollectionViewFlowLayout

-- Create the PinchFlowLayout class as a subclass of UICollectionViewFlowLayout
local PinchFlowLayout = class.createClass ("PichFlowLayout", UICollectionViewFlowLayout)

-- Do the handling of the pinch gesture attributes here

function PinchFlowLayout:applyPinchToLayoutAttributes (layoutAttributes)
    
    if layoutAttributes.indexPath:isEqual (self.pinchedCellPath) then
        
        -- This is the pinched cell
        layoutAttributes.center = self.pinchedCellCenter
        layoutAttributes.zIndex = 1
        
        local transform3D = CATransform3D.Identity
        
        local scale = math.min (self._pinchedCellScale, 4.1)
        if scale ~= nil then
            transform3D = CATransform3D.Scale (transform3D, scale, scale, 1)
        end
        
        -- Uncomment the lines below if you want the celles to rotate when pinched
        --[[ if self.rotationAngle ~= nil then
            transform3D = CATransform3D.Rotate (transform3D, self.rotationAngle, 0, 0, 1)
        end]]
        
        layoutAttributes.transform3D = transform3D
    end
    
end

-- Overwrite the layoutAttributes methods of the UICollectionViewFlowLayout class

function PinchFlowLayout:layoutAttributesForElementsInRect (rect)
    
    -- call super
    local allAttributesInRect = self:doAsClass ("layoutAttributesForElementsInRect", UICollectionViewFlowLayout, rect)
    
    -- If a pinch gesture is active, handle it
    if self._pinchedCellCenter ~= nil then 
        
        -- as allAttributesInRect is an Objective C NSArray object, we can use it in a for loop, just as a Lua array
        -- similarly we can use a NSDictionnary object in a for loop (with pairs in place of ipairs)
        for i, cellAttributes in ipairs(allAttributesInRect) do
            self:applyPinchToLayoutAttributes (cellAttributes);
        end
    end
    
    return allAttributesInRect;
end

function PinchFlowLayout:layoutAttributesForItemAtIndexPath (indexPath)
   
    -- call super
   local attributes = self:doAsClass ("layoutAttributesForItemAtIndexPath", UICollectionViewFlowLayout, indexPath)
   
    -- If a pinch gesture is active, handle it
    if self._pinchedCellCenter ~= nil then 
        self:applyPinchToLayoutAttributes (attributes);
    end
   
   return attributes;
end

-- Declare getters and setters for the pinch gesture parameters

PinchFlowLayout:declareSetters { pinchedCellScale = function (self, scale)
                                                        self._pinchedCellScale = scale
                                                        -- recalculate and redraw the layout when the scale is changed
                                                        self:invalidateLayout ()
                                                    end,
                                 pinchedCellCenter = function (self, origin)
                                                         self._pinchedCellCenter = origin
                                                         -- recalculate and redraw the layout when the center is changed
                                                         self:invalidateLayout ()
                                                     end,
                                 rotationAngle = function (self, angle)
                                                     self._rotationAngle = angle
                                                     -- recalculate and redraw the layout when the rotation angle is changed
                                                     self:invalidateLayout ()
                                                 end,
                               }

PinchFlowLayout:declareGetters { pinchedCellScale = function (self) return self._pinchedCellScale end,
                                 pinchedCellCenter = function (self) return self._pinchedCellCenter end,
                                 rotationAngle = function (self) return self._rotationAngle end, 
                               }

return PinchFlowLayout