-- Celedev Collectionview demo app
-- Example of a all-in-Lua iOS application using Celdev Responsive Development Environment
-- Created by Jean-Luc Jumpertz - Celedev on 20/10/2013

-- Create the CollectionController class
local ControllerClass = require "CollectionController"

-- Create a ViewController instance
viewController = ControllerClass:new()

-- Subscribe to the system message "system.did_load_module": 
-- On each module reload, the method viewController:refresh() will be called
message.defaultCenter:addSubscriberForMessage ("system.did_load_module", viewController, "refresh")

return viewController